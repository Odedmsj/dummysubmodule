using UnityEngine;

public class SomeName : MonoBehaviour
{
    public Rigidbody2D prefab;

    private void Awake()
    {
        Debug.Log("Some Logs");
    }

    internal void DoSome()
    {
        var obj = Instantiate(prefab, Vector3.zero, Quaternion.identity);
        obj.name = "COPY";
    }
}
